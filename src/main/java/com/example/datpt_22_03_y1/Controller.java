package com.example.datpt_22_03_y1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/bt1")
public class Controller {
    private final ServiceImpl service;

    @GetMapping("/signIn")
    public ResponseEntity<?> signIn (@RequestParam String userName,@RequestParam String password) {
        return new ResponseEntity<>(service.signIn(userName, password), HttpStatus.OK);
    }

    @GetMapping("/checkToken")
    public ResponseEntity<?> checkToken (@RequestParam String token) {
        return new ResponseEntity<>(service.checkToken(token), HttpStatus.OK);
    }
}
