package com.example.datpt_22_03_y1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Datpt2203Y1Application {

    public static void main(String[] args) {
        SpringApplication.run(Datpt2203Y1Application.class, args);
    }

}
