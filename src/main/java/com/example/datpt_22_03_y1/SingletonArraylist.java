package com.example.datpt_22_03_y1;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SingletonArraylist {
    private static SingletonArraylist instance;
    private ArrayList<String> arrayList;
    private Timer timer;

    private SingletonArraylist() {
        arrayList = new ArrayList<>();
        // Khởi tạo timer để lên lịch loại bỏ phần tử từ ArrayList mỗi 1 phút
        timer = new Timer();
        timer.schedule(new RemoveElementTask(), 0, 60000); // Lên lịch cho nhiệm vụ loại bỏ mỗi 1 phút (60000 milliseconds)
    }

    public static SingletonArraylist getInstance() {
        if (instance == null) {
            instance = new SingletonArraylist();
        }
        return instance;
    }

    public void addElement(String element) {
        arrayList.add(element);
    }

    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    private class RemoveElementTask extends TimerTask {
        @Override
        public void run() {
            // Loại bỏ phần tử đầu tiên của ArrayList (nếu có)
            if (arrayList.size() == 0) {
                System.out.println("Không có token!");
                return;
            }
            for (int i = 0; i < arrayList.size(); i++ ) {
                String [] parts = arrayList.get(i).split("_");
                long expirationTimeMillis = Long.parseLong(parts[1]);
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis > expirationTimeMillis) {
                    arrayList.remove(i);
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
    }
}
